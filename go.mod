module gitlab.com/wanna-lib/auto-bank

go 1.19

require (
	github.com/gofiber/fiber v1.14.6
	gitlab.com/trendgolibrary/trend-call v1.0.3
)

require (
	github.com/andybalholm/brotli v1.0.0 // indirect
	github.com/gofiber/utils v0.0.10 // indirect
	github.com/gorilla/schema v1.1.0 // indirect
	github.com/klauspost/compress v1.10.7 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.16.0 // indirect
	github.com/valyala/tcplisten v0.0.0-20161114210144-ceec8f93295a // indirect
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
)
