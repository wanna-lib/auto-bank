package autobank

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	hauto "gitlab.com/wanna-lib/auto-bank/helper"
	"strings"
	"time"
)

type EnableData struct {
	Id             int         `json:"id"`
	AccountName    string      `json:"account_name"`
	AccountNumber  string      `json:"account_number"`
	Pin            string      `json:"pin"`
	CurrentBalance float64     `json:"current_balance"`
	CreatedAt      interface{} `json:"created_at"`
	UpdatedAt      time.Time   `json:"updated_at"`
	Active         int         `json:"active"`
	BankCode       string      `json:"bank_code"`
	Idcard         string      `json:"idcard"`
	Phonenumber    string      `json:"phonenumber"`
	Birthday       string      `json:"birthday"`
	TransferActive interface{} `json:"transfer_active"`
}

type EnableResponse struct {
	Result  bool       `json:"result"`
	Message string     `json:"message"`
	Data    EnableData `json:"data"`
}

func (c context) Enable(bankID string, token string, response *EnableResponse) Context {
	payload := strings.NewReader(
		fmt.Sprintf(
			"bank_id=%s",
			bankID,
		),
	)

	headers := map[string]string{
		fiber.HeaderAuthorization: "Bearer " + token,
		fiber.HeaderContentType:   "application/x-www-form-urlencoded",
	}
	reqParam := requests.Params{
		URL:     hauto.JoinStr(c.cfg.Host, `/api/bank/enable`),
		HEADERS: headers,
		BODY:    payload,
		TIMEOUT: 30,
	}

	var res requests.Response
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if res.Code != 200 {
		var errorMessage ErrorMessage
		if err := json.Unmarshal(res.Result, &errorMessage); err != nil {
			return c.handleError(err)
		}
		return c.handleError(fmt.Errorf(errorMessage.Message))
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}
	return &c
}
