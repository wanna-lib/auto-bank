package autobank

import (
	hauto "gitlab.com/wanna-lib/auto-bank/helper"
	"testing"
)

var CTX = Init(Configuration{
	Host:     "",
	Username: "",
	Password: "",
})

func TestLogin(t *testing.T) {
	var responseLogin LoginResponse
	if err := CTX.Login(&responseLogin).Error(); err != nil {
		hauto.PrintStructJson(err)
		panic(err)
	}
	hauto.PrintStructJson(responseLogin)
}

func TestProfile(t *testing.T) {
	var responseLogin LoginResponse
	if err := CTX.Login(&responseLogin).Error(); err != nil {
		panic(err)
	}

	var responseProfile ProfileResponse
	if err := CTX.Profile(responseLogin.AccessToken, &responseProfile).Error(); err != nil {
		panic(err)
	}
	hauto.PrintStructJson(responseProfile)
}

func TestRegister(t *testing.T) {
	var responseLogin LoginResponse
	if err := CTX.Login(&responseLogin).Error(); err != nil {
		panic(err)
	}

	var responseRegister RegisterResponse
	if err := CTX.Register(RegisterRequest{
		CardId:      "1102001042598",
		PhoneNumber: "0658302065",
		BirthDay:    "1988-10-23",
		Pin:         "033859",
		Token:       responseLogin.AccessToken,
	}, &responseRegister).Error(); err != nil {
		panic(err)
	}
	hauto.PrintStructJson(responseRegister)
}

func TestOTP(t *testing.T) {
	var responseLogin LoginResponse
	if err := CTX.Login(&responseLogin).Error(); err != nil {
		panic(err)
	}

	var responseOTP OTPResponse
	if err := CTX.OTP(OTPRequest{
		PhoneNumber: "0658302065",
		Pin:         "033859",
		OTP:         "872964",
		ApiAuth:     "a324929d-40af-474c-b956-68f0a0c92766",
		TokenUUID:   "ydNZZibeaR",
		Tag:         "500405fa26908b227f402a08b06516e21b1fa5e2ce6fc843cfc0df1551714ab2e06951ae19f076b2e1a6f794a147368be0e9033cda24256649c6748acd88250d425d04771eb1d05b1ee2098b1c876832242a0b84c4ec126d492ffa5a278d4f51bc13f8774c390bbcbf91d8213e377d7ddac5b6517e5bd6c22bbfd7fa659bde09e9f7f8504afabdb993f487bc5a581daadb6714bea37f2695691648e486c17367e2f2d8756d9915bb38c73521b5a51c72de0bf26fab03b5dfe97352c78fbcd0c7",
		Token:       responseLogin.AccessToken,
	}, &responseOTP).Error(); err != nil {
		panic(err)
	}
	hauto.PrintStructJson(responseOTP)
}

func TestBankList(t *testing.T) {
	var responseLogin LoginResponse
	if err := CTX.Login(&responseLogin).Error(); err != nil {
		panic(err)
	}

	var responseBankList BankListResponse
	if err := CTX.BankList(responseLogin.AccessToken, &responseBankList).Error(); err != nil {
		panic(err)
	}
	hauto.PrintStructJson(responseBankList)
}

func TestTransactionList(t *testing.T) {
	var responseLogin LoginResponse
	if err := CTX.Login(&responseLogin).Error(); err != nil {
		panic(err)
	}

	var responseTransactionList TransactionListResponse
	if err := CTX.TransactionList(TransactionListRequest{
		BankID: "3",
		Token:  responseLogin.AccessToken,
	}, &responseTransactionList).Error(); err != nil {
		panic(err)
	}
	hauto.PrintStructJson(responseTransactionList)
}

func TestTransfer(t *testing.T) {
	//var responseLogin LoginResponse
	//if err := CTX.Login(&responseLogin).Error(); err != nil {
	//	panic(err)
	//}

	var responseTransfer TransferResponse
	if err := CTX.Transfer(TransferRequest{
		BankId:         "7",
		TargetBankCode: "SCB",
		TargetBankNo:   "7782288497",
		Amount:         "1",
		Token:          "13|INX48BAXDVaZKn9pt1bTq52TB4HmatsBzN9KWwwU",
	}, &responseTransfer).Error(); err != nil {
		panic(err)
	}
	hauto.PrintStructJson(responseTransfer)
}

func TestEnable(t *testing.T) {
	var responseLogin LoginResponse
	if err := CTX.Login(&responseLogin).Error(); err != nil {
		panic(err)
	}

	var responseEnable EnableResponse
	if err := CTX.Enable("3", responseLogin.AccessToken, &responseEnable).Error(); err != nil {
		panic(err)
	}
	hauto.PrintStructJson(responseEnable)
}

func TestDisable(t *testing.T) {
	var responseLogin LoginResponse
	if err := CTX.Login(&responseLogin).Error(); err != nil {
		panic(err)
	}

	var responseDisable DisableResponse
	if err := CTX.Disable("3", responseLogin.AccessToken, &responseDisable).Error(); err != nil {
		panic(err)
	}
	hauto.PrintStructJson(responseDisable)
}
