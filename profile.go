package autobank

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	hauto "gitlab.com/wanna-lib/auto-bank/helper"
	"time"
)

type ProfileResponse struct {
	Id              int         `json:"id"`
	Name            string      `json:"name"`
	Email           string      `json:"email"`
	EmailVerifiedAt interface{} `json:"email_verified_at"`
	CreatedAt       time.Time   `json:"created_at"`
	UpdatedAt       time.Time   `json:"updated_at"`
	ApiToken        interface{} `json:"api_token"`
	Username        string      `json:"username"`
	Status          int         `json:"status"`
	Partner         int         `json:"partner"`
}

func (c context) Profile(token string, response *ProfileResponse) Context {
	headers := map[string]string{
		fiber.HeaderAuthorization: "Bearer " + token,
	}
	reqParam := requests.Params{
		URL:     hauto.JoinStr(c.cfg.Host, `/api/profile`),
		HEADERS: headers,
		TIMEOUT: 30,
	}

	var res requests.Response
	if err := requests.Call().Get(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if res.Code != 200 {
		var errorMessage ErrorMessage
		if err := json.Unmarshal(res.Result, &errorMessage); err != nil {
			return c.handleError(err)
		}
		return c.handleError(fmt.Errorf(errorMessage.Message))
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}
	return &c
}
