package autobank

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	hauto "gitlab.com/wanna-lib/auto-bank/helper"
	"strings"
	"time"
)

type TransferStatus struct {
	Code        int    `json:"code"`
	Header      string `json:"header"`
	Description string `json:"description"`
}

type PaymentInfo struct {
	Type        string      `json:"type"`
	Title       string      `json:"title"`
	Header      interface{} `json:"header"`
	Description interface{} `json:"description"`
	ImageURL    interface{} `json:"imageURL"`
	QRstring    string      `json:"QRstring"`
}

type AdditionalMetaData struct {
	PaymentInfo []PaymentInfo `json:"paymentInfo"`
}

type TransferData struct {
	TransactionId       string             `json:"transactionId"`
	TransactionDateTime time.Time          `json:"transactionDateTime"`
	RemainingBalance    float64            `json:"remainingBalance"`
	Status              interface{}        `json:"status"`
	OrgRquid            interface{}        `json:"orgRquid"`
	AdditionalMetaData  AdditionalMetaData `json:"additionalMetaData"`
}

type TransferDataResult struct {
	Status TransferStatus `json:"status"`
	Data   TransferData   `json:"data"`
}

type TransferResponse struct {
	Result  bool               `json:"result"`
	Code    int                `json:"code"`
	Message string             `json:"message"`
	Data    TransferDataResult `json:"data"`
}

type TransferRequest struct {
	BankId         string `json:"bank_id"`
	TargetBankCode string `json:"target_bank_code"`
	TargetBankNo   string `json:"target_bank_no"`
	Amount         string `json:"amount"`
	Token          string `json:"token"`
}

func (c context) Transfer(req TransferRequest, response *TransferResponse) Context {
	str := fmt.Sprintf(
		"bank_id=%s&target_bank_code=%s&target_bank_no=%s&amount=%s",
		req.BankId, req.TargetBankCode, req.TargetBankNo, req.Amount,
	)
	fmt.Println(str)
	payload := strings.NewReader(str)

	headers := map[string]string{
		fiber.HeaderAuthorization: "Bearer " + req.Token,
		fiber.HeaderContentType:   "application/x-www-form-urlencoded",
	}
	reqParam := requests.Params{
		URL:     hauto.JoinStr(c.cfg.Host, `/api/bank/transfer`),
		HEADERS: headers,
		BODY:    payload,
		TIMEOUT: 30,
	}

	var res requests.Response
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}
	if res.Code != 200 {
		//var errorMessage map[string]interface{}
		//if err := json.Unmarshal(res.Result, &errorMessage); err != nil {
		//	return c.handleError(err)
		//}
		return c.handleError(fmt.Errorf(string(res.Result)))
	}

	var decode string
	if err := hauto.DeleteTextInJson(res.Result, &decode); err != nil {
		return c.handleError(fmt.Errorf(err.Error()))
	}

	if err := json.Unmarshal([]byte(decode), &response); err != nil {
		return c.handleError(err)
	}
	return &c
}
