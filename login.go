package autobank

import (
	"encoding/json"
	"fmt"
	requests "gitlab.com/trendgolibrary/trend-call"
	hauto "gitlab.com/wanna-lib/auto-bank/helper"
	"strings"
)

type LoginResponse struct {
	Message     string `json:"message"`
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
}

func (c context) Login(response *LoginResponse) Context {
	payload := strings.NewReader(fmt.Sprintf("username=%s&password=%s", c.cfg.Username, c.cfg.Password))
	headers := map[string]string{
		"content-type": "application/x-www-form-urlencoded",
	}
	reqParam := requests.Params{
		URL:     hauto.JoinStr(c.cfg.Host, `/api/login`),
		HEADERS: headers,
		BODY:    payload,
		TIMEOUT: 30,
	}

	var res requests.Response
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if res.Code != 200 {
		var errorMessage ErrorMessage
		if err := json.Unmarshal(res.Result, &errorMessage); err != nil {
			return c.handleError(err)
		}
		return c.handleError(fmt.Errorf(errorMessage.Message))
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}
	return &c
}
