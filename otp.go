package autobank

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	hauto "gitlab.com/wanna-lib/auto-bank/helper"
	"strings"
)

type OTPData struct {
	Id             int         `json:"id"`
	AccountName    string      `json:"account_name"`
	AccountNumber  string      `json:"account_number"`
	Pin            string      `json:"pin"`
	CurrentBalance float64     `json:"current_balance"`
	CreatedAt      interface{} `json:"created_at"`
	UpdatedAt      interface{} `json:"updated_at"`
	Active         int         `json:"active"`
	BankCode       string      `json:"bank_code"`
	Idcard         string      `json:"idcard"`
	Phonenumber    string      `json:"phonenumber"`
	Birthday       string      `json:"birthday"`
}
type OTPResponse struct {
	Result  bool    `json:"result"`
	Message string  `json:"message"`
	Data    OTPData `json:"data"`
}

type OTPRequest struct {
	PhoneNumber string `json:"phone_number"`
	Pin         string `json:"pin"`
	OTP         string `json:"otp"`
	ApiAuth     string `json:"apiAuth"`
	TokenUUID   string `json:"tokenUUID"`
	Tag         string `json:"tag"`
	Token       string `json:"token"`
}

func (c context) OTP(req OTPRequest, response *OTPResponse) Context {
	payload := strings.NewReader(
		fmt.Sprintf(
			"phoneNumber=%s&pin=%s&apiAuth=%s&tokenUUID=%s&otp=%s&tag=%s",
			req.PhoneNumber, req.Pin, req.ApiAuth, req.TokenUUID, req.OTP, req.Tag,
		),
	)

	headers := map[string]string{
		fiber.HeaderAuthorization: "Bearer " + req.Token,
		fiber.HeaderContentType:   "application/x-www-form-urlencoded",
	}
	reqParam := requests.Params{
		URL:     hauto.JoinStr(c.cfg.Host, `/api/bank/otp`),
		HEADERS: headers,
		BODY:    payload,
		TIMEOUT: 30,
	}

	var res requests.Response
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if res.Code != 200 {
		var errorMessage ErrorMessage
		if err := json.Unmarshal(res.Result, &errorMessage); err != nil {
			return c.handleError(err)
		}
		return c.handleError(fmt.Errorf(errorMessage.Message))
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}
	return &c
}
