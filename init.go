package autobank

type Configuration struct {
	Host     string
	Username string
	Password string
}

type ErrorMessage struct {
	Result  bool   `json:"result"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type (
	Context interface {
		Login(response *LoginResponse) Context
		Profile(token string, response *ProfileResponse) Context
		Register(req RegisterRequest, response *RegisterResponse) Context
		OTP(req OTPRequest, response *OTPResponse) Context
		BankList(token string, response *BankListResponse) Context
		TransactionList(req TransactionListRequest, response *TransactionListResponse) Context
		Transfer(req TransferRequest, response *TransferResponse) Context
		Enable(bankID string, token string, response *EnableResponse) Context
		Disable(bankID string, token string, response *DisableResponse) Context
		Error() error
	}
	context struct {
		cfg Configuration
		err error
	}
)

func (c *context) Error() error {
	if c.err != nil {
		return c.err
	}
	return nil
}

func Init(conf Configuration) Context {
	return &context{
		cfg: conf,
	}
}

func (c context) handleError(err error) Context {
	c.err = err
	return &c
}
