package autobank

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	hauto "gitlab.com/wanna-lib/auto-bank/helper"
	"strings"
)

type TransactionListRemark struct {
	Bank   string `json:"bank"`
	Number string `json:"number"`
	Name   string `json:"name"`
}

type TransactionListChannel struct {
	Code        string `json:"code"`
	Description string `json:"description"`
}

type TransactionListType struct {
	Code        string `json:"code"`
	Description string `json:"description"`
}

type Transaction struct {
	DateTime       string                 `json:"dateTime"`
	Amount         float64                `json:"amount"`
	TxRemark       string                 `json:"txRemark"`
	TxHash         string                 `json:"txHash"`
	Remark         TransactionListRemark  `json:"remark"`
	Detail         string                 `json:"detail"`
	Channel        TransactionListChannel `json:"channel"`
	Type           TransactionListType    `json:"type"`
	NextPageNumber interface{}            `json:"nextPageNumber"`
}

type TransactionListData struct {
	AvailableBalance float64       `json:"availableBalance"`
	Transactions     []Transaction `json:"transactions"`
}

type TransactionListResponse struct {
	Result  bool                `json:"result"`
	Code    int                 `json:"code"`
	Message string              `json:"message"`
	Data    TransactionListData `json:"data"`
}

type TransactionListRequest struct {
	BankID string `json:"bank_id"`
	Token  string `json:"token"`
}

func (c context) TransactionList(req TransactionListRequest, response *TransactionListResponse) Context {
	payload := strings.NewReader(
		fmt.Sprintf(
			"bank_id=%s",
			req.BankID,
		),
	)

	headers := map[string]string{
		fiber.HeaderAuthorization: "Bearer " + req.Token,
		fiber.HeaderContentType:   "application/x-www-form-urlencoded",
	}
	reqParam := requests.Params{
		URL:     hauto.JoinStr(c.cfg.Host, `/api/bank/transaction/list`),
		HEADERS: headers,
		BODY:    payload,
		TIMEOUT: 30,
	}

	var res requests.Response
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if res.Code != 200 {
		var errorMessage ErrorMessage
		if err := json.Unmarshal(res.Result, &errorMessage); err != nil {
			return c.handleError(err)
		}
		return c.handleError(fmt.Errorf(errorMessage.Message))
	}

	var decode string
	if err := hauto.DeleteTextInJson(res.Result, &decode); err != nil {
		return c.handleError(fmt.Errorf(err.Error()))
	}

	if err := json.Unmarshal([]byte(decode), &response); err != nil {
		return c.handleError(err)
	}
	return &c
}
