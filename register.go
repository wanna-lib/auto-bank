package autobank

import (
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
	hauto "gitlab.com/wanna-lib/auto-bank/helper"
	"strings"
)

type RegisterData struct {
	ApiAuth   string `json:"apiAuth"`
	Pac       string `json:"pac"`
	TokenUUID string `json:"tokenUUID"`
	Tag       string `json:"tag"`
}
type RegisterResponse struct {
	Result  bool         `json:"result"`
	Message string       `json:"message"`
	Data    RegisterData `json:"data"`
	Status  int          `json:"status"`
}

type RegisterRequest struct {
	CardId      string `json:"card_id"`
	PhoneNumber string `json:"phone_number"`
	BirthDay    string `json:"birth_day"`
	Pin         string `json:"pin"`
	Token       string `json:"token"`
}

func (c context) Register(req RegisterRequest, response *RegisterResponse) Context {
	payload := strings.NewReader(
		fmt.Sprintf(
			"cardId=%s&phoneNumber=%s&birthDay=%s&pin=%s",
			req.CardId, req.PhoneNumber, req.BirthDay, req.Pin,
		),
	)

	headers := map[string]string{
		fiber.HeaderAuthorization: "Bearer " + req.Token,
		fiber.HeaderContentType:   "application/x-www-form-urlencoded",
	}
	reqParam := requests.Params{
		URL:     hauto.JoinStr(c.cfg.Host, `/api/bank/add`),
		HEADERS: headers,
		BODY:    payload,
		TIMEOUT: 30,
	}

	var res requests.Response
	if err := requests.Call().Post(reqParam, &res).Error(); err != nil {
		return c.handleError(err)
	}

	if res.Code != 200 {
		var errorMessage ErrorMessage
		if err := json.Unmarshal(res.Result, &errorMessage); err != nil {
			return c.handleError(err)
		}
		return c.handleError(fmt.Errorf(errorMessage.Message))
	}

	if err := json.Unmarshal(res.Result, &response); err != nil {
		return c.handleError(err)
	}
	return &c
}
