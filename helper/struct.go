package hauto

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
)

func CopyStructToStruct(target interface{}, result interface{}) error {
	fooByte, err := json.Marshal(&target)
	if err != nil {
		return err
	}

	err = json.Unmarshal(fooByte, result)
	if err != nil {
		return err
	}
	return nil
}

func PrintStructJson(target interface{}) {
	fooByte, _ := json.MarshalIndent(&target, "", "\t")
	fmt.Println(string(fooByte))
}

func HashSha256(cid string) string {
	cidHash := sha256.Sum256([]byte(cid))
	return hex.EncodeToString(cidHash[:])
}
