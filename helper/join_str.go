package hauto

import (
	"encoding/json"
	"fmt"
	"strings"
)

func JoinStr(v ...string) string {
	str := ""
	for _, i := range v {
		str += i
	}
	return str
}

func DeleteTextInJson(res []byte, strRes *string) error {
	inputStr := string(res)
	startIndex := strings.Index(inputStr, "{")
	if startIndex == -1 {
		fmt.Println("No JSON found in the input string.")
		return fmt.Errorf(`No JSON found in the input string.`)
	}

	// Extract the JSON part of the string
	jsonStr := inputStr[startIndex:]

	// Attempt to unmarshal the extracted JSON string into a map[string]interface{}
	var jsonData map[string]interface{}
	err := json.Unmarshal([]byte(jsonStr), &jsonData)
	if err != nil {
		fmt.Println("Error:", err)
		fmt.Println("Extracted JSON is not valid.")
		return err
	}

	fmt.Println("Extracted JSON:")
	fmt.Println(jsonStr)
	*strRes = jsonStr
	return nil
}
